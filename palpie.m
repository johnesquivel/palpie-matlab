function varargout = palpie(varargin)
% PALPIE MATLAB code for palpie.fig
%      PALPIE, by itself, creates a new PALPIE or raises the existing
%      singleton*.
%
%      H = PALPIE returns the handle to a new PALPIE or the handle to
%      the existing singleton*.
%
%      PALPIE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PALPIE.M with the given input arguments.
%
%      PALPIE('Property','Value',...) creates a new PALPIE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before palpie_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to palpie_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help palpie

% Last Modified by GUIDE v2.5 29-Jan-2014 16:45:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @palpie_OpeningFcn, ...
                   'gui_OutputFcn',  @palpie_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before palpie is made visible.
function palpie_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to palpie (see VARARGIN)

% Choose default command line output for palpie
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes palpie wait for user response (see UIRESUME)
% uiwait(handles.figure1);
initialize_gui(hObject, handles, false);


% --- Outputs from this function are returned to the command line.
function varargout = palpie_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;  

% Power  or log conversion function
function [outImg] = filterFunc(handles,fType,pwrBase)
    % Pull in and normalize the input image
    inImg = im2double(handles.FormData.origImg);
    if strcmpi(fType,'pwr')
        % Power function case
        newImg = inImg .^ pwrBase;
    elseif strcmpi(fType,'log')
        % Log function case
        newImg = log(inImg+1.0)/log(pwrBase);
    end
    outImg = im2uint8(newImg);
    
    
 % Update function curve
function updateCurve(handles,fType,pwrBase)
    X = linspace(0,1); % Generate X
    if strcmpi(fType,'pwr') % Power case
        Y = X .^ pwrBase;
    elseif strcmpi(fType,'log')  % Log case
        Y = log(X+1.0)/log(pwrBase);
    end
    % Plot the fuction
    axis(handles.axes2);
    plot(handles.axes2,X,Y,'r');
    set(handles.axes2,'Xgrid','on','Ygrid','on');
 
 % Update label
function updateLabel(hObject,handles,fType,pwrBase)
    if strcmpi(fType,'pwr')
        % Power case
        set(handles.logText,'Visible','off')
        set(handles.baseText,'FontSize',14);
        set(handles.baseText,'String','X');
        pwrStr = sprintf('%0.1f', pwrBase);
        % Update label
        set(handles.pwrText,'String',pwrStr);
    elseif strcmpi(fType,'log')
        % Log case
        set(handles.logText,'Visible','on')
        set(handles.baseText,'FontSize',12);
        baseStr = sprintf('%0.1f', pwrBase);
        % Update label
        set(handles.baseText,'String',baseStr);
        set(handles.pwrText,'String','X');
    end
    
% --- Executes on button press in openButton.
function openButton_Callback(hObject, eventdata, handles)
    mytitle = 'Open Input Image';
    [file,path] = uigetfile({'*.jpg;*.tif;*.png;*.gif','All Image Files';...
          '*.*','All Files' },'mytitle');
    origImg = imread([path file]);
    handles.FormData.origImg = origImg;
    % Save any data placed in handles
    guidata(hObject,handles);
    % Plot it on axes 1:
    axes(handles.axes1); % Switches focus to this axes object.
    imshow(origImg); % Display image
    % Calc new image
    fType = handles.FormData.fType;
    pwrBase = handles.FormData.pwrBase;
    newImg = filterFunc(handles,fType,pwrBase);
    % Plot new image
    axes(handles.axes3); % Switches focus to this axes object.
    imshow(newImg); % Display image
    % Save any data placed in handles
    guidata(hObject,handles);
    
    
% --- Executes on button press in upButton.
function upButton_Callback(hObject, eventdata, handles)
    fType = handles.FormData.fType;
    pwrBase = handles.FormData.pwrBase + 0.1;  % Increase pwr
    updateLabel(hObject,handles,fType,pwrBase);  % update the label
    updateCurve(handles,fType,pwrBase);
    % Calc new image
    newImg = filterFunc(handles,fType,pwrBase);
    % Plot new image
    axes(handles.axes3); % Switches focus to this axes object.
    imshow(newImg); % Display image
    % Save any data placed in handles
    handles.FormData.fType = fType;
    handles.FormData.pwrBase = pwrBase;
    guidata(hObject,handles);


    
% --- Executes on button press in downButton.
function downButton_Callback(hObject, eventdata, handles)
    fType = handles.FormData.fType;
    pwrBase = handles.FormData.pwrBase - 0.1;  % Increase pwr
    updateLabel(hObject,handles,fType,pwrBase);  % update the label
    updateCurve(handles,fType,pwrBase);
    % Calc new image
    newImg = filterFunc(handles,fType,pwrBase);
    % Plot new image
    axes(handles.axes3); % Switches focus to this axes object.
    imshow(newImg); % Display image
    % Save any data placed in handles
    handles.FormData.fType = fType;
    handles.FormData.pwrBase = pwrBase;
    guidata(hObject,handles);
    


% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, eventdata, handles)
    fileName = uiputfile({'*.jpg;*.tif;*.png;*.gif', ...
        'All Image Files'; '*.*','All Files' }, ...
        'Save Output Image');
    if fileName ~= 0
        imwrite(handles.FormData.origImg,fileName);
    end
    


% --- Executes on button press in powerRadioButton.
function powerRadioButton_Callback(hObject, eventdata, handles)
    set(handles.powerRadioButton,'Value',1);
    set(handles.logRadioButton,'Value',0);
    fType = 'pwr';
    pwrBase = 2.0;
    updateLabel(hObject,handles,fType,pwrBase);
    updateCurve(handles,fType,pwrBase);
    newImg = filterFunc(handles,fType,pwrBase);
    % Plot new image
    axes(handles.axes3); % Switches focus to this axes object.
    imshow(newImg); % Display image
    % Save any data placed in handles
    handles.FormData.fType = fType;
    handles.FormData.pwrBase = pwrBase;
    guidata(hObject,handles);


% --- Executes on button press in logRadioButton.
function logRadioButton_Callback(hObject, eventdata, handles)
    set(handles.logRadioButton,'Value',1);
    set(handles.powerRadioButton,'Value',0);
    fType = 'log';
    pwrBase = 2.0;
    updateLabel(hObject,handles,fType,pwrBase);
    updateCurve(handles,fType,pwrBase);
    newImg = filterFunc(handles,fType,pwrBase);
    % Plot new image
    axes(handles.axes3); % Switches focus to this axes object.
    imshow(newImg); % Display image
    % Save any data placed in handles
    handles.FormData.fType = fType;
    handles.FormData.pwrBase = pwrBase;
    guidata(hObject,handles);


% --------------------------------------------------------------------
function initialize_gui(hObject, handles, isreset)
% If the FormData field is present and the reset flag is false, it means
% we are we are just re-initializing a GUI by calling it from the cmd line
% while it is up. So, bail out as we dont want to reset the data.
    if isfield(handles, 'FormData') && ~isreset
        return;
    end
    % Set icon image & arrows
    dlf = imread('palpieIcon.png');
    image(dlf,'Parent',handles.iconAxes);
    uaT = imread('U.png');
    set(handles.upButton,'Cdata',uaT);
    daT = imread('D.png');
    set(handles.downButton,'Cdata',daT);
    
    % Set initial function to power = 1.0
    set(handles.powerRadioButton,'Value',1);
    fType = 'pwr';
    pwrBase = 2.0;
    % Update the curve and labels
    updateLabel(hObject,handles,fType,pwrBase);
    updateCurve(handles,fType,pwrBase);
    % Save any data placed in handles
    handles.FormData.fType = fType;
    handles.FormData.pwrBase = pwrBase;
    guidata(hObject,handles);
    
    % Turn off all axis scales
    axis(handles.iconAxes,'off');
    axis(handles.axes1,'off');
    axis(handles.axes3,'off');
