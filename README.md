# README #

PALPIE - MATLAB

### What is this repository for? ###

* PALPIE is a example of an image manipulation GUI.
* This example applies either the log or power transform.
* This is the MATLAB version 0.9

### How do I get set up? ###

* To run this example, download the built example.
* To run from source, clone the repository then run in MATLAB