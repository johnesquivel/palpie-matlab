function myPlot()
    x = linspace(0,1);
    y1 = x .^ (1.5);
    y2 = x .^ (1.8);
    y3 = x .^ (2.0);
    y4 = x .^ (2.2);
    y5 = x .^ (3.0);

    l1 = logA(1.5,x);
    l2 = logA(2.0,x);
    l3 = logA(3.0,x);

    figure
    subplot(2,1,1); 
    plot(x,y1,x,y2,x,y3,x,y4,x,y5)
    grid('on')
    subplot(2,1,2); 
    plot(x,l1,x,l2,x,l3)
    grid('on')
end


function [output] = logA(b, x)
    output = log(x) / log(b);
end
